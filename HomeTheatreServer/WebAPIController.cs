﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using HomeTheatreServer.Models;

namespace HomeTheatreServer
{
    public class WebAPIController
    {

        // this is where we will send it
        string commandURL = "http://localhost:13579/command.html";
        string statusURL = "http://localhost:13579/status.html";
        string referer;
        string last_url;
        int retry_counts = 5;
        string returnstring = "";
        public static string current_time = "";
        string[] videoUrls;
        //List<string> downVideoUrls = new List<string>();

        public string POST_CMD(int cmd, string rest = "")
        {
            try
            {
                //Console.WriteLine ("d="+date+"&t="+time+"&v1="+watt_hours+"&v2="+watts+"&v5="+celcius+"&v6="+volts);
                // this is what we are sending
                string post_data = "wm_command=" + cmd.ToString() + rest;
                //Console.WriteLine(post_data);
                // create a request
                HttpWebRequest request = (HttpWebRequest)
                    WebRequest.Create(commandURL);
                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version10;
                request.Method = "POST";
                //request.Headers.Add("X-Pvoutput-Apikey", "24071b850fcf32699e66637f3d043e5762979894");
                //request.Headers.Add("X-Pvoutput-SystemId", "14730");


                // turn our request string into a byte stream
                byte[] postBytes = Encoding.ASCII.GetBytes(post_data);

                // this is important - make sure you specify type this way
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postBytes.Length;
                Stream requestStream = request.GetRequestStream();

                // now send it
                requestStream.Write(postBytes, 0, postBytes.Length);
                requestStream.Close();

                // grab te response and print it out to the console along with the status code
                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                //!Console.WriteLine(new StreamReader(response.GetResponseStream()).ReadToEnd());
                //!Console.WriteLine(response.StatusCode);
                return "Post data:" + post_data + " " + response.StatusCode.ToString() + "\n";
            }
            catch (Exception)
            {
                return "Can't Send! did you close Media Player?";
            }
        }

        private VideoInfo GET_CMD()
        {
            try
            {
                var info = new VideoInfo();

                var wrGETURL = WebRequest.Create(statusURL);
                var myProxy = new WebProxy("myproxy", 80);
                myProxy.BypassProxyOnLocal = true;

                //wrGETURL.Proxy = WebProxy.GetDefaultProxy();

                var objStream = wrGETURL.GetResponse().GetResponseStream();
                var objReader = new StreamReader(objStream);

                var sLine = "";
                var i = 0;

                while (sLine != null)
                {
                    i++;
                    sLine = objReader.ReadLine();
                    if (sLine != null)
                        returnstring += sLine + "\n";
                }
                // Вырезаем информацию о предыдущем состоянии, потому что мы его и так знаем
                returnstring = returnstring.Substring(returnstring.IndexOf("OnStatus(") + 9);
                returnstring = returnstring.Substring(0, returnstring.Length - 2);
                // Убираем лишние символы
                //Console.WriteLine(returnstring);

                var splitreturn = returnstring.Split(new[] { "\", " , ", \"", ", "}, StringSplitOptions.RemoveEmptyEntries);


                //Console.WriteLine("__BEGIN");
                for (int index = 0; index < splitreturn.Length; index++)
                {
                    var s = splitreturn[index] = splitreturn[index].Replace("\"", "");
                    //Console.WriteLine("." + s + ".");
                }
                //Console.WriteLine("Размер ИНФЫ: " + splitreturn.Length);
                //Console.WriteLine("__END");
                

                info.FileName = splitreturn[0];
                info.Status = "Пауза".Equals(splitreturn[1]) ? PlayStatus.pausing : PlayStatus.playing;
                info.Position = int.Parse(splitreturn[2]);
                info.CurrentTime = splitreturn[3];
                info.Duration = int.Parse(splitreturn[4]);
                info.TotalTime = splitreturn[5];
                info.Path = splitreturn[8];
                return info;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /*
        private void textBox3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            POST_CMD(-1, "&position=" + textBox3.Text);
        }
        */
        private void vol_plus_button(object sender, EventArgs e)
        {
            POST_CMD(907);
        }

        private void vol_min_button(object sender, EventArgs e)
        {
            POST_CMD(908);
        }

        public void pauseButton()
        {
            POST_CMD(888);
        }

        public void playButton()
        {
            POST_CMD(887);
        }

        public void prevAudio()
        {
            POST_CMD(953);
        }

        public void nextAudio()
        {
            POST_CMD(952);
        }

        public void prevSub()
        {
            POST_CMD(955);
        }

        public void nextSub()
        {
            POST_CMD(954);
        }

        public VideoInfo getInfo()
        {
            return GET_CMD();
        }

        public void setPosition(int position)
        {
            POST_CMD(-1, "&position=" + position);
        }

        /*
        private void button9_Click(object sender, EventArgs e)
        {
            Program.MPC_target = textBox12.Text + ":" + textBox13.Text;
        }
        */

        /*
        private void timer1_Tick(object sender, EventArgs e)
        {

            textBox2.Text = GET_CMD();
            if (textBox10.Text != "")
            {
                int total_seconds = (int)(Convert.ToUInt64(TimeSpan.Parse(textBox10.Text).TotalSeconds));
                trackBar2.Maximum = total_seconds;
            }
            if (textBox8.Text != "")
            {
                int current_seconds = (int)(Convert.ToUInt64(TimeSpan.Parse(textBox8.Text).TotalSeconds));
                trackBar2.Value = current_seconds;
            }
        }
        */


        private void button12_Click(object sender, EventArgs e)
        {
            Process[] prs = Process.GetProcesses();
            foreach (Process pr in prs)
            {
                if (pr.ProcessName.Contains("mpc-hc"))
                {
                    pr.Kill();
                }

            }
        }


        public void playToolStripMenuItem_Click()
        {
            Console.WriteLine(POST_CMD(887));
        }

        public void pauseToolStripMenuItem_Click()
        {
            Console.WriteLine(POST_CMD(888));
        }

    }
}