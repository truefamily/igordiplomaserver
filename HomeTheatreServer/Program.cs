﻿using MPC_API_LIB;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using HomeTheatreServer.DAO;
using HomeTheatreServer.Utils;


namespace HomeTheatreServer
{
    class Program
    {
        // Признак, указывающий, выводится ли звук на аудиопанель
        private static bool isSoundOnPanel = false;
        // Список сериалов
        private static string[] seriesList;
        // Список фильмов
        private static string[] movieList;
        // экземпляр MPC
        private static MPC mpc;
        
        private static string nowPlaying;

        static void Main(string[] args)
        {
            //SqlDao dao = new SqlDao();
            //dao.createDbFileIfNotExist();
            //dao.connect();
            //dao.test();
            Console.Title = "HomeTheatreServer";

            var pass = ConfigurationManager.AppSettings["password"];
            if (pass == null || "".Equals(pass))
            {
                Console.WriteLine("В настройках не задан пароль. Сервер остановлен.");
                Console.ReadLine();
                return;
            }

            //var caPrivKey = CryptoUtils.GenerateCACertificate("CN=root ca");
            //var cert = CryptoUtils.GenerateSelfSignedCertificate("CN=192.168.1.12", "CN=root ca", caPrivKey);
            //CryptoUtils.addCertToStore(cert, StoreName.My, StoreLocation.CurrentUser);


            var ip = IPAddress.Parse(ConfigurationManager.AppSettings["ip"]);
            var port = int.Parse(ConfigurationManager.AppSettings["port"]);
            HttpServer httpServer = new MyHttpServer(ip, port);
            var thread = new Thread(httpServer.Listen);
            thread.Start();
            Console.WriteLine("Сервер запущен по адресу " + ip + ":" + port);
            thread.Join();
        }

    }
}
