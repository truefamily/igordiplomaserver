﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace HomeTheatreServer.Utils
{
    public abstract class HttpServer
    {
        protected IPAddress Ip;
        protected int Port;
        TcpListener _listener;
        readonly bool _isActive = true;

        protected HttpServer(IPAddress ip, int port)
        {
            Ip = ip;
            Port = port;
        }

        public void Listen()
        {
            //var caPrivKey = CryptoUtils.GenerateCACertificate("CN=HomeTheatreServer CA");
            //var serverCertificate = CryptoUtils.GenerateSelfSignedCertificate("CN=192.168.1.12", "CN=HomeTheatreServer CA", caPrivKey);

            _listener = new TcpListener(Ip, Port);
            _listener.Start();
            while (_isActive)
            {
                var tcpClient = _listener.AcceptTcpClient();
                /*
                using (var sslStream = new SslStream(tcpClient.GetStream(), false, App_CertificateValidation))
                {
                    //sslStream.AuthenticateAsServer(serverCertificate, true, SslProtocols.Tls12, false);
                    
                    var processor = new HttpProcessor(tcpClient, this, sslStream);
                    var thread = new Thread(processor.Process);
                    thread.Start();
                    Thread.Sleep(1);
                }
                */
                var processor = new HttpProcessor(tcpClient, this, null);
                var thread = new Thread(processor.Process);
                thread.Start();
                Thread.Sleep(1);
            }
        }

        bool App_CertificateValidation(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None) { return true; }
            if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors) { return true; } //we don't have a proper certificate tree
            return false;
        }

        public abstract void HandleGetRequest(HttpProcessor p);
        public abstract int HandlePostRequest(HttpProcessor p, StreamReader inputData);
    }
}
