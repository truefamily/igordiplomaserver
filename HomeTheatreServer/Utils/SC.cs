﻿using System.IO;

namespace HomeTheatreServer.Utils
{
    class SC
    {
        //public static string SETTINGS_FILENAME = Directory.GetCurrentDirectory() + @"\settings.ini";
        // Название программы для изменения вывода звука
        public static string SOUND_CHANGER_APP_NAME = "DefSound.exe";

        // Ключи настроек
        //public static string MAIN_SECTION = "main_settings";
        public static string SERIES_PATH = "series_path";
        public static string MOVIES_PATH = "movies_path";
        public static string MAIN_AUDIO = "main_audio";
        public static string ALT_AUDIO = "alternative_audio";
        public static string MPC_HC_PATH = "media_player_classic_hc_path";

        // Команды от клиента
        public static string SERIES = "series";
        public static string MOVIE = "movies";
    }
}
