﻿using System;
using System.Collections;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;

namespace HomeTheatreServer.Utils
{
    public class HttpProcessor
    {
        public TcpClient Socket;
        public HttpServer Server;
        public SslStream sslStream;

        private Stream _inputStream;
        public StreamWriter OutputStream;

        public string HttpMethod;
        public string HttpUrl;
        public string HttpProtocolVersionstring;
        public Hashtable HttpHeaders = new Hashtable();


        private const int MAX_POST_SIZE = 10*1024*1024; // 10MB

        public HttpProcessor(TcpClient socket, HttpServer server, SslStream sslStream)
        {
            Socket = socket;
            Server = server;
            this.sslStream = sslStream;
        }

        private static string StreamReadLine(Stream inputStream)
        {
            var data = "";
            while (true)
            {
                var nextChar = inputStream.ReadByte();
                if (nextChar == '\n') break;
                if (nextChar == '\r') continue;
                if (nextChar == -1)
                {
                    Thread.Sleep(1);
                    continue;
                }
                data += Convert.ToChar(nextChar);
            }
            return data;
        }

        public void Process()
        {
            _inputStream = new BufferedStream(Socket.GetStream());
            OutputStream = new StreamWriter(new BufferedStream(Socket.GetStream()));
            var exitCode = 0;
            try
            {
                ParseRequest();
                ReadHeaders();
                if (HttpMethod.Equals("GET"))
                {
                    HandleGetRequest();
                }
                else if (HttpMethod.Equals("POST"))
                {
                    exitCode = HandlePostRequest();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
                WriteFailure();
            }
            OutputStream.Flush();
            _inputStream = null; OutputStream = null;
            Socket.Close();
            if (exitCode == 1)
            {
                // Завершаем работу приложения
                Environment.Exit(1);
            }
            if (exitCode == 2)
            {
                // Завершаем работу компьютера
                System.Diagnostics.Process.Start("shutdown", "/s /t 0");
            }
        }

        public void ParseRequest()
        {
            var request = StreamReadLine(_inputStream);
            var tokens = request.Split(' ');
            if (tokens.Length != 3)
            {
                throw new Exception("invalid http request line");
            }
            HttpMethod = tokens[0].ToUpper();
            HttpUrl = tokens[1];
            HttpProtocolVersionstring = tokens[2];
            //Console.WriteLine("starting: " + request);
        }

        public void ReadHeaders()
        {
            string line;
            while ((line = StreamReadLine(_inputStream)) != null)
            {
                if (line.Equals(""))
                {
                    return;
                }
                var separator = line.IndexOf(':');
                if (separator == -1)
                {
                    throw new Exception("invalid http header line: " + line);
                }
                var name = line.Substring(0, separator);
                var pos = separator + 1;
                while ((pos < line.Length) && (line[pos] == ' '))
                {
                    pos++; // выделяем пробелы
                }

                var value = line.Substring(pos, line.Length - pos);
                //Console.WriteLine("header: {0}:{1}", name, value);
                HttpHeaders[name] = value;
            }
        }

        public void HandleGetRequest()
        {
            Server.HandleGetRequest(this);
        }

        private const int BUF_SIZE = 4096;

        public int HandlePostRequest()
        {
            var ms = new MemoryStream();
            if (!HttpHeaders.ContainsKey("Content-Length")) return Server.HandlePostRequest(this, new StreamReader(ms));

            var contentLen = Convert.ToInt32(this.HttpHeaders["Content-Length"]);
            if (contentLen > MAX_POST_SIZE)
            {
                throw new Exception($"POST Content-Length({contentLen}) too big for this simple server");
            }
            var buf = new byte[BUF_SIZE];
            var toRead = contentLen;
            while (toRead > 0)
            {
                // Console.WriteLine("starting Read, to_read={0}", to_read);
                var numread = this._inputStream.Read(buf, 0, Math.Min(BUF_SIZE, toRead));
                //Console.WriteLine("read finished, numread={0}", numread);
                if (numread == 0)
                {
                    if (toRead == 0)
                    {
                        break;
                    }
                    throw new Exception("client disconnected during post");
                }
                toRead -= numread;
                ms.Write(buf, 0, numread);
            }
            ms.Seek(0, SeekOrigin.Begin);
            return Server.HandlePostRequest(this, new StreamReader(ms));
        }

        public void WriteSuccess(string contentType = "text/html")
        {
            OutputStream.WriteLine("HTTP/1.0 200 OK");
            // Записываем HTTP-заголовки        
            OutputStream.WriteLine("Content-Type: " + contentType);
            OutputStream.WriteLine("Connection: close");
            // это завершаем часть заголовков, после этого идет HTTP-body
            OutputStream.WriteLine("");
        }

        public void WriteFailure()
        {
            // Записываем ответ с 404 ошибкой
            OutputStream.WriteLine("HTTP/1.0 404 File not found");
            OutputStream.WriteLine("Connection: close");
            OutputStream.WriteLine("");
        }
    }
}
