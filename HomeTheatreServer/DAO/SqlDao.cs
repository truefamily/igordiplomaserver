﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using HomeTheatreServer.Models;
using HomeTheatreServer.Utils;

namespace HomeTheatreServer.DAO
{
    public class SqlDao
    {
        private const string DB_NAME = "HomeTheatre.sqlite";
        private const string CONNECTION_STRING = "Data Source=HomeTheatre.sqlite;Version=3;";

        public void createDbFileIfNotExist()
        {
            if (!File.Exists(DB_NAME))
            {
                SQLiteConnection.CreateFile(DB_NAME);
            }
        }

        private const string DROP_TABLE_HISTORY_LOG = "DROP TABLE IF EXISTS history_log";
        private const string CREATE_TABLE_HISTORY_LOG = "CREATE TABLE IF NOT EXISTS history_log " + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " + "device_id varchar(17) NOT NULL, " + "action varchar(300) NOT NULL, " + "filename varchar(300), " + "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL)";

        private const string DROP_TABLE_HISTORY_WATCHED = "DROP TABLE IF EXISTS history_watched";
        private const string CREATE_TABLE_HISTORY_WATCHED = "CREATE TABLE IF NOT EXISTS history_watched " + "(device_id varchar(17) NOT NULL, " + "filename varchar(300) NOT NULL, " + "duration int NOT NULL, " + "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, " + "PRIMARY KEY(filename))";

        private const string DROP_TABLE_NOT_FINISHED = "DROP TABLE IF EXISTS not_finished";

        private const string CREATE_TABLE_NOT_FINISHED = "CREATE TABLE IF NOT EXISTS not_finished " + "(device_id varchar(17) NOT NULL, " + "filename varchar(300) NOT NULL, " + "position int NOT NULL, " + "timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, " + "PRIMARY KEY(filename))";

        public void test()
        {
            //TODO удалить
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                var command = new SQLiteCommand(DROP_TABLE_HISTORY_LOG, connect);
                command.ExecuteNonQuery();
                command = new SQLiteCommand(DROP_TABLE_HISTORY_WATCHED, connect);
                command.ExecuteNonQuery();
                command = new SQLiteCommand(DROP_TABLE_NOT_FINISHED, connect);
                command.ExecuteNonQuery();


                command = new SQLiteCommand(CREATE_TABLE_HISTORY_LOG, connect);
                command.ExecuteNonQuery();
                command = new SQLiteCommand(CREATE_TABLE_HISTORY_WATCHED, connect);
                command.ExecuteNonQuery();
                command = new SQLiteCommand(CREATE_TABLE_NOT_FINISHED, connect);
                command.ExecuteNonQuery();
            }
        }


        public bool addLog(string @deviceId, Commands @action)
        {
            return addLog(@deviceId, @action, null);
        }

        public bool addLog(string @deviceId, Commands @action, string @filename)
        {
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                var insertSql =
                    new SQLiteCommand(
                        "INSERT INTO history_log (device_id, action, filename, timestamp) VALUES (@deviceId,@action,@filename,@timestamp)",
                        connect);
                insertSql.Parameters.Add(new SQLiteParameter("@deviceId", deviceId));
                insertSql.Parameters.Add(new SQLiteParameter("@action", action.ToString()));
                insertSql.Parameters.Add(new SQLiteParameter("@filename", filename));
                insertSql.Parameters.Add(new SQLiteParameter("@timestamp", DateTime.Now));
                try
                {
                    return insertSql.ExecuteNonQuery() > 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool addNotFinishedVideo(string @deviceId, string @filename, int position)
        {
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                var insertSql =
                    new SQLiteCommand(
                        "INSERT OR REPLACE INTO not_finished (device_id, filename, position, timestamp) VALUES (@deviceId,@filename,@position,@timestamp)",
                        connect);
                insertSql.Parameters.Add(new SQLiteParameter("@deviceId", deviceId));
                insertSql.Parameters.Add(new SQLiteParameter("@filename", filename));
                insertSql.Parameters.Add(new SQLiteParameter("@position", position));
                insertSql.Parameters.Add(new SQLiteParameter("@timestamp", DateTime.Now));
                try
                {
                    return insertSql.ExecuteNonQuery() > 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public NotFinishedVideo getNotFinishedVideo(string @filename)
        {
            var extVideoInfo = new NotFinishedVideo();
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                using (var fmd = connect.CreateCommand())
                {
                    fmd.CommandText = @"SELECT position FROM not_finished WHERE filename = @fileName";
                    fmd.CommandType = CommandType.Text;
                    fmd.Parameters.Add(new SQLiteParameter("@fileName", @filename));
                    var r = fmd.ExecuteReader();
                    while (r.Read())
                    {
                        extVideoInfo.Position = Convert.ToInt32(r["position"]);
                    }
                }
            }
            return extVideoInfo;
        }

        public int deleteNotFinishedVideoRecord(string @filename)
        {
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                var insertSql = new SQLiteCommand("DELETE FROM not_finished WHERE filename = @filename", connect);
                insertSql.Parameters.Add(new SQLiteParameter("@filename", filename));
                try
                {
                    return insertSql.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public bool addHistoryRecord(string @deviceId, string @filename, int duration)
        {
            using (var connect = new SQLiteConnection(CONNECTION_STRING))
            {
                connect.Open();
                var insertSql =
                    new SQLiteCommand(
                        "INSERT OR REPLACE INTO history_watched (device_id, filename, duration, timestamp) VALUES (@deviceId,@filename,@duration,@timestamp)",
                        connect);
                insertSql.Parameters.Add(new SQLiteParameter("@deviceId", deviceId));
                insertSql.Parameters.Add(new SQLiteParameter("@filename", filename));
                insertSql.Parameters.Add(new SQLiteParameter("@duration", duration));
                insertSql.Parameters.Add(new SQLiteParameter("@timestamp", DateTime.Now));
                try
                {
                    return insertSql.ExecuteNonQuery() > 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
