﻿using System;

namespace HomeTheatreServer.Models
{
    public class NotFinishedVideo
    {
        private VideoInfo info;
        private int position;
        private DateTime time;

        public VideoInfo Info
        {
            get { return info; }
            set { info = value; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
    }
}
