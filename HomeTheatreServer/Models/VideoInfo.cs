﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheatreServer.Models
{
    public class VideoInfo
    {
        private string fileName;
        private PlayStatus status;
        private int position;
        private int duration;
        private string path;
        private string currentTime;
        private string totalTime;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public PlayStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public string CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; }
        }

        public string TotalTime
        {
            get { return totalTime; }
            set { totalTime = value; }
        }

        public override string ToString()
        { 
            return "[filename=" + fileName +
                ", status=" + status +
                ", position=" + position +
                ", duration=" + duration + 
                ", path=" + path + 
                ", currentTime="+ currentTime +
                ", totalTime=" + totalTime + "]";
        }
    }
}
