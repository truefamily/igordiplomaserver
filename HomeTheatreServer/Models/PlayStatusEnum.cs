﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTheatreServer.Models
{
    public enum PlayStatus
    {
        pausing = 1,
        playing = 2,
        undefined = 3
    }

}
