﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using HomeTheatreServer.DAO;
using HomeTheatreServer.Models;
using HomeTheatreServer.Utils;
using MPC_API_LIB;
using Newtonsoft.Json;

namespace HomeTheatreServer
{
    public class MyHttpServer : HttpServer
    {
        // Признак, указывающий, выводится ли звук на аудиопанель
        private static bool isSoundOnPanel;
        // Экземпляр MPC
        private static MPC mpc;
        // Экземпляр контроллера MPC
        private static WebAPIController webApi;
        // Информация о текущем файле
        private static VideoInfo Info;
        private static bool lastDeleteStatus;

        public static SqlDao dao = new SqlDao();

        private static readonly string SERIES_PATH = ConfigurationManager.AppSettings[SC.SERIES_PATH];
        private static readonly string MOVIES_PATH = ConfigurationManager.AppSettings[SC.MOVIES_PATH];

        private static string nowPlaying;

        public MyHttpServer(IPAddress ip, int port)
            : base(ip, port)
        {
            webApi = new WebAPIController();
            //dao.createDbFileIfNotExist();
            //Console.WriteLine("Created db.");
            //dao.test();
        }

        public override void HandleGetRequest(HttpProcessor p)
        {
            p.WriteSuccess();
            p.OutputStream.WriteLine("<html><body><h1>Use POST for interaction</h1>");
        }

        public override int HandlePostRequest(HttpProcessor p, StreamReader inputData)
        {
            var data = inputData.ReadToEnd();
            var query = HttpUtility.ParseQueryString(data);
            var res = performCommand(query);
            p.WriteSuccess();
            if ("1".Equals(res))
            {
                return 1;
            }
            if ("2".Equals(res))
            {
                return 2;
            }
            p.OutputStream.Write(res);
            return 0;
        }

        private string performCommand(NameValueCollection args)
        {
            bool recursively;
            var path = SERIES_PATH;

            // Получаем пароль доступа из запроса
            var requestAuth = args.Get("password");
            if (requestAuth != null)
            {
                if (!requestAuth.Equals(ConfigurationManager.AppSettings["password"]))
                {
                    return GetErrorMessage("Пароли не совпадают, проверьте настройки.");
                }
            }
            else
            {
                return GetErrorMessage("В запросе отсутствует пароль.");
            }
            // Идентификатор устройства, с которого пришел запрос
            var identifier = args.Get("identifier");
            // Команда из запроса клиента
            var command = args.Get("command");
            switch ((Commands)int.Parse(command))
            {
                case Commands.Sound:
                    if (isSoundOnPanel)
                    {
                        SetSoundToDynamics();
                    }
                    else
                    {
                        SetSoundToPanel();
                    }
                    dao.addLog(identifier, Commands.Sound);
                    return GetOkStatusMessage();

                case Commands.SeriesList:
                    path = SERIES_PATH;
                    recursively = bool.Parse(args.Get("recursively"));
                    var seriesList = GetFilesList(path, recursively);
                    dao.addLog(identifier, Commands.SeriesList);
                    return ListOfNamesToJson(seriesList);

                case Commands.MoviesList:
                    path = MOVIES_PATH;
                    recursively = bool.Parse(args.Get("recursively"));
                    var moviesList = GetFilesList(path, recursively);
                    dao.addLog(identifier, Commands.MoviesList);
                    return ListOfNamesToJson(moviesList);

                case Commands.Settings:
                    dao.addLog(identifier, Commands.Settings, Info.FileName);
                    return InfoToJson();

                case Commands.Run:
                    var mpcThread = new Thread(StartMpc);
                    recursively = bool.Parse(args.Get("recursively"));
                    var initialPath = SC.SERIES.Equals(args.Get("type")) ? SERIES_PATH : MOVIES_PATH;
                    var opt = recursively ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                    var fileName = args.Get("fileName");
                    var file = Directory.GetFiles(initialPath, fileName, opt).First();
                    if (file == null)
                    {
                        return GetErrorMessage("Файл не найден.");
                    }
                    dao.addLog(identifier, Commands.Run, file);
                    mpcThread.Start(file);
                    mpcThread.Join();

                    return RunResultInJson(fileName);

                case Commands.PlayPause:
                    var t = new Thread(PlayPause);
                    dao.addLog(identifier, Commands.PlayPause, Info.FileName);
                    t.Start();
                    t.Join();
                    return InfoToJson();

                case Commands.Close:
                    new Thread(closeMpc).Start();
                    dao.addLog(identifier, Commands.Close, Info.FileName);
                    return GetOkStatusMessage();

                case Commands.CloseDelete:
                    var currentCloseDeleteInfo = webApi.getInfo();
                    var closeDeleteThread = new Thread(CloseAndDelete);
                    dao.addLog(identifier, Commands.CloseDelete, Info.FileName);
                    dao.addHistoryRecord(identifier, currentCloseDeleteInfo.FileName, currentCloseDeleteInfo.Duration);
                    closeDeleteThread.Start();
                    return GetOkStatusMessage();

                case Commands.CloseSave:
                    var currentCloseSaveInfo = webApi.getInfo();
                    new Thread(CloseAndSavePos).Start();
                    dao.addLog(identifier, Commands.CloseSave, Info.FileName);
                    dao.addNotFinishedVideo(identifier, currentCloseSaveInfo.FileName, currentCloseSaveInfo.Position);
                    return GetOkStatusMessage();

                case Commands.Stop:
                    //TODO STOP?
                    break;

                case Commands.IncVolume:
                    new Thread(IncreaseVolume).Start();
                    dao.addLog(identifier, Commands.IncVolume, Info.FileName);
                    return InfoToJson();

                case Commands.DecVolume:
                    new Thread(DecreaseVolume).Start();
                    dao.addLog(identifier, Commands.DecVolume, Info.FileName);
                    return InfoToJson();

                case Commands.FullScreen:
                    new Thread(ToggleFullScreen).Start();
                    dao.addLog(identifier, Commands.FullScreen, Info.FileName);
                    return GetOkStatusMessage();

                case Commands.Forward:
                    new Thread(Forward).Start(args.Get("seconds"));
                    dao.addLog(identifier, Commands.Forward, Info.FileName);
                    return GetOkStatusMessage();

                case Commands.SetPosition:
                    var setPosThread = new Thread(SetPosition);
                    setPosThread.Start(args.Get("position"));
                    dao.addLog(identifier, Commands.SetPosition, Info.FileName);
                    setPosThread.Join();
                    return InfoToJson();

                case Commands.BackWard:
                    new Thread(Backward).Start(args.Get("seconds"));
                    dao.addLog(identifier, Commands.BackWard, Info.FileName);
                    return GetOkStatusMessage();

                case Commands.Refresh:
                    dao.addLog(identifier, Commands.Refresh, Info.FileName);
                    return InfoToJson();

                case Commands.ChangeVolume:
                    new Thread(ChangeVolume).Start(args.Get("direction") + "|" + args.Get("isSys"));
                    dao.addLog(identifier, Commands.ChangeVolume, Info.FileName);
                    return InfoToJson();

                case Commands.Mute:
                    new Thread(mute).Start();
                    dao.addLog(identifier, Commands.Mute, Info.FileName);
                    return InfoToJson();

                case Commands.CloseServer:
                    Console.WriteLine("Shutdowning the app..");
                    dao.addLog(identifier, Commands.CloseServer);
                    return "1";

                case Commands.TurnOffPC:
                    Console.WriteLine("Shutdowning the PC..");
                    dao.addLog(identifier, Commands.TurnOffPC);
                    return "2";

                case Commands.PrevSub:
                    dao.addLog(identifier, Commands.PrevSub, Info.FileName);
                    webApi.prevSub();
                    return InfoToJson();

                case Commands.NextSub:
                    dao.addLog(identifier, Commands.NextSub, Info.FileName);
                    webApi.nextSub();
                    return InfoToJson();

                case Commands.PrevAudio:
                    dao.addLog(identifier, Commands.PrevAudio, Info.FileName);
                    webApi.prevAudio();
                    return InfoToJson();

                case Commands.NextAudio:
                    dao.addLog(identifier, Commands.NextAudio, Info.FileName);
                    webApi.nextAudio();
                    return InfoToJson();

                default:
                    throw new ArgumentOutOfRangeException();
            }
            return null;
        }

        /* 
        *
        * Методы для управления MPC 
        *
        */
        private static void RefreshVideoInfo()
        {
            Info = webApi.getInfo();
        }

        /* Запустить экземпляр MPC */
        private static void StartMpc(object fileName)
        {
            Info = null;
            mpc = new MPC();
            mpc.Run(ConfigurationManager.AppSettings[SC.MPC_HC_PATH]);
            mpc.Run();
            mpc.OpenFile((string) fileName);
            mpc.Run();
            Console.WriteLine("Запуск " + fileName);
        }

        /* Переключить в/из режима fullScreen */
        private static void ToggleFullScreen()
        {
            if (IsMpcNull()) return;
            mpc.ToggleFullScreen();
            Console.WriteLine("Полноэкранный/обычный режим");
        }

        /* Переключить в/из режима fullScreen */
        private static void mute()
        {
            if (IsMpcNull()) return;
            SysMute();
            Console.WriteLine("Отключение громкости");
        }

        /* Увеличить громкость */
        private static void IncreaseVolume()
        {
            if (IsMpcNull()) return;
            mpc.IncreaseVolume();
            Console.WriteLine("Увеличение громкости");
        }

        /* Уменьшить громкость */
        private static void DecreaseVolume()
        {
            if (IsMpcNull()) return;
            mpc.DecreaseVolume();
            Console.WriteLine("Уменьшение громкости");
        }

        /* Изменить громкость */
        private static void ChangeVolume(object args)
        {
            if (IsMpcNull()) return;

            var volArgs = ((string) args).Split('|');
            var isSysBool = bool.Parse(volArgs[1]);
            if (isSysBool)
            {
                ChangeSysVolume("inc".Equals(volArgs[0]));
            }
            else
            {
                if ("inc".Equals(volArgs[0]))
                {
                    mpc.IncreaseVolume();
                }
                else
                {
                    mpc.DecreaseVolume();
                }
            }
            Console.WriteLine("Изменение громкости");
        }

        /* Пауза/воспроизведение */
        private static void PlayPause()
        {
            if (IsMpcNull()) return;
            switch (Info.Status)
            {
                case PlayStatus.playing:
                    webApi.pauseButton();
                    break;
                case PlayStatus.pausing:
                    webApi.playButton();
                    break;
                case PlayStatus.undefined:
                    Console.WriteLine("Состояние не определено, команда будет пропущена");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            //mpc.PlayPause();
            Console.WriteLine("Воспроизведение/пауза");
        }

        /* Установить позицию воспроизведения */
        private static void SetPosition(object seconds)
        {
            if (IsMpcNull()) return;
            var time = int.Parse((string) seconds)/1000;
            mpc.SetPosition(time);
            Console.WriteLine("Установка позиции на " + (string) seconds + " секунд");
        }

        /* Перемотка вперед */
        private static void Forward(object seconds)
        {
            if (IsMpcNull()) return;
            mpc.JumpForward(int.Parse((string) seconds));
            Console.WriteLine("Перемотка вперед на " + (string) seconds + " секунд");
        }

        /* Перемотка назад */
        private static void Backward(object seconds)
        {
            if (IsMpcNull()) return;
            mpc.JumpBackward(int.Parse((string) seconds));
            Console.WriteLine("Перемотка назад на " + (string) seconds + " секунд");
        }

        /* Закрыть MPC */
        private static void closeMpc()
        {
            if (IsMpcNull()) return;
            mpc.CloseMPC();
            mpc = null;
            Console.WriteLine("Закрытие плеера");
        }

        /* Закрыть и удалить */
        private static void CloseAndDelete()
        {
            if (IsMpcNull())
            {
                lastDeleteStatus = false;
                return;
            }
            Info = webApi.getInfo();
            closeMpc();
            Thread.Sleep(100);
            if (!File.Exists(Info.Path))
            {
                Console.WriteLine(Info.Path);
                Console.WriteLine("Файл не существует.");
                lastDeleteStatus = false;
                return;
            }
            for (var numTries = 0; numTries < 10; numTries++)
            {
                try
                {
                    File.Delete(Info.Path);
                    lastDeleteStatus = true;
                }
                catch (IOException)
                {
                    Thread.Sleep(100);
                }
            }

            Console.WriteLine("Файл удален: " + Info.Path);
            if (dao.deleteNotFinishedVideoRecord(Info.FileName) > 0)
            {
                Console.WriteLine("Записи о незавершенном просмотре данного файла удалены");
            }
        }

        /* Закрыть и сохранить позицию */
        private static void CloseAndSavePos()
        {
            if (IsMpcNull()) return;
            closeMpc();
            Console.WriteLine("Закрытие и сохранение позиции");
        }

        /* Получить список серий (абсолютные пути) */
        private static string[] GetFilesList(string path, bool recursively)
        {
            var opt = recursively ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            return Directory.EnumerateFiles(path, "*.*", opt).Where(s => !s.Contains(',') && (s.EndsWith(".mp4") || s.EndsWith(".avi") || s.EndsWith(".mkv"))).OrderBy(e => e).ToArray();
        }

        /* Переключить вывод звука на аудиопанель */
        private static void SetSoundToPanel()
        {
            Process.Start(SC.SOUND_CHANGER_APP_NAME, "\"" + ConfigurationManager.AppSettings[SC.ALT_AUDIO] + "\", ALL");
            isSoundOnPanel = true;
            Console.WriteLine("Переключение звука на альтернативный аудиовывод");
        }

        /* Переключить вывод звука на динамики компьютера */
        private static void SetSoundToDynamics()
        {
            Process.Start(SC.SOUND_CHANGER_APP_NAME, "\"" + ConfigurationManager.AppSettings[SC.MAIN_AUDIO] + "\", ALL");
            isSoundOnPanel = false;
            Console.WriteLine("Переключение звука на основной аудиовывод");
        }

        /* Проверить состояние плеера */
        private static bool IsMpcNull()
        {
            return mpc == null;
        }

        /* Отправить список имен файлов */
        private static string ListOfNamesToJson(string[] list)
        {
            var fileNames = new string[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                var splitted = list[i].Split('\\');
                fileNames[i] = splitted[splitted.Length - 1];
            }

            var json = JsonConvert.SerializeObject(new {fileNames});
            Console.WriteLine("Отправлен список файлов, количество файлов: " + fileNames.Length);
            return json;
        }

        private static string InfoToJson()
        {
            RefreshVideoInfo();
            return Info != null ? JsonConvert.SerializeObject(new {Info}) : GetErrorMessage("Взаимодействие с видеоплеером закончилось неудачей");
        }

        private static string RunResultInJson(string fileName)
        {
            var i = 0;
            while (Info == null || !Info.FileName.Equals(fileName))
            {
                RefreshVideoInfo();
                Thread.Sleep(500);
                i++;
                if (i > 20) break;
            }
            var extVideoInfo = dao.getNotFinishedVideo(fileName);
            extVideoInfo.Info = Info;
            return JsonConvert.SerializeObject(extVideoInfo);
        }

        private static string GetOkStatusMessage()
        {
            var error = new Dictionary<string, string> {{"status", "OK"}};
            var response = JsonConvert.SerializeObject(error);
            //Console.WriteLine(response);
            return response;
        }


        private static string GetErrorMessage(string msg)
        {
            var error = new Dictionary<string, string> {{"error", msg}};
            var response = JsonConvert.SerializeObject(error);
            //Console.WriteLine(response);
            return response;
        }

        public static void SysMute()
        {
            try
            {
                var MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                var DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.All);
                foreach (var dev in DevCol)
                {
                    try
                    {
                        if (dev.State != NAudio.CoreAudioApi.DeviceState.Active) continue;
                        dev.AudioEndpointVolume.Mute = !dev.AudioEndpointVolume.Mute;
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при перечислении аудиоустройств: " + ex.Message);
            }
        }

        public static void ChangeSysVolume(bool isUp)
        {
            try
            {
                var MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                var DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.All);
                foreach (var dev in DevCol)
                {
                    try
                    {
                        if (dev.State != NAudio.CoreAudioApi.DeviceState.Active) continue;
                        if (isUp)
                        {
                            dev.AudioEndpointVolume.VolumeStepUp();
                            continue;
                        }
                        dev.AudioEndpointVolume.VolumeStepDown();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Во время перебора аудиоустройств возникло исключение: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Во время перебора аудиоустройств возникло исключение: " + ex.Message);
            }
        }
    }
}
