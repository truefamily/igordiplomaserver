﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace HomeTheatreServer
{
    class DeviceChangeNotifier : Form
    {
        public delegate void DeviceNotifyDelegate(Message msg);
        public static event DeviceNotifyDelegate DeviceNotify;
        private static DeviceChangeNotifier mInstance;

        public static void Start()
        {
            Thread t = new Thread(runForm);
            t.SetApartmentState(ApartmentState.STA);
            t.IsBackground = true;
            t.Start();
        }
        public static void Stop()
        {
            if (mInstance == null) throw new InvalidOperationException("Notifier not started");
            DeviceNotify = null;
            mInstance.Invoke(new MethodInvoker(mInstance.endForm));
        }
        private static void runForm()
        {
            Application.Run(new DeviceChangeNotifier());
        }

        private void endForm()
        {
            this.Close();
        }
        protected override void SetVisibleCore(bool value)
        {
            // Prevent window getting visible
            if (mInstance == null) CreateHandle();
            mInstance = this;
            value = false;
            base.SetVisibleCore(value);
        }
        protected override void WndProc(ref Message m)
        {
            Console.WriteLine("NOTIFIER Hwnd=" + m.HWnd);
            Console.WriteLine("NOTIFIER LParam=" + m.LParam);
            Console.WriteLine("NOTIFIER WParam=" + m.WParam);
            Console.WriteLine("NOTIFIER msg=" + m.Msg);
            // Trap WM_DEVICECHANGE
            /*
            if (m.Msg == 0x219)
            {
                DeviceNotifyDelegate handler = DeviceNotify;
                if (handler != null) handler(m);
            }
            */
            base.WndProc(ref m);
        }
    }
}
